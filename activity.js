

let http = require("http");

http.createServer(function(request,response){

	console.log(request.url);

	if(request.url === "/"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Welcome to B176 Booking System");
	} else if (request.url === "/courses"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Welcome to the courses Page. View our courses.")
	}else if (request.url === "/profile"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Welcome to your Profile. View your details")
	} else {
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end("Resource cannot be found.")
	}
}).listen(5000);

console.log("Server is running on localhost: 5000")